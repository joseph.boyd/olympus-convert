# olympus-convert

Uses [bftools](https://downloads.openmicroscopy.org/bio-formats/6.13.0/artifacts/bftools.zip) to convert slides in Olympus format (.vsi/.ets) to openslide-compatible OME-TIFF format.

## Preparation

In practice, it may be convenient to create a single directory of syslinks to the Olympus dataset. Then, a file `process_list.csv` should be prepared following:

```
<IN_FILE1>,<OUT_FILE1>,<SERIES1>
<IN_FILE2>,<OUT_FILE2>,<SERIES2>
<IN_FILE3>,<OUT_FILE3>,<SERIES3>
```

where `<IN_FILE>` and `<OUT_FILE>` are the absolute paths to the `.vsi` and output file (must use `.ome.tiff` extension). The series is the index of the slide within the Olympus stack. This should usually be set to 6, as the overview/thumbnail image constitutes the first 6 indices of the stack and should be discarded. In some cases, additional images are included in stack, and determining the series parameter requires discretion. The [PathML](https://github.com/Dana-Farber-AIOS/pathml/tree/master) library can be used to determine the series parameter, along with the following code:

```
import os
import glob
import numpy as np
from pathml.core import SlideData

for file_path in glob.glob('*.vsi'):
    print(os.path.basename(file_path))
    try:
        wsi = SlideData(file_path)
        level = np.argmax([dims[0] for dims in wsi.slide.shape_list])
        print(level)
        print(wsi.slide.shape_list)
    except:
        print('An error occurred...')
```

N.B. despite being able to read Olympus files, PathML does not seem to be able to convert them.

## Usage

The script calls the `bfconvert` script from `bftools`, so this must be available in the current directory. The script is best run as a job array with as many processes as possible. No coordination is required between the processes; a process will simply process the next slide that has not yet begun to export.

```
sbatch --array=0-29 -J array slurm_submit.sh
```

The options for compression are the following: "LZW", "JPEG-2000", "JPEG-2000 Lossy", "JPEG", "zlib". The option "JPEG-2000 Lossy" appears to yield the smallest file sizes, while providing a visual quality very close to those of lossless compressions.

