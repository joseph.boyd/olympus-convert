#!/bin/bash
#SBATCH --job-name=bfconvert
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=mem
#SBATCH --mem=80000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam

while IFS="," read -r file_in file_out series
do
    yes n | BF_MAX_MEM=50G ./bfconvert -series $series \
                                       -tilex 256 \
                                       -tiley 256 \
                                       -pyramid-resolutions 6 \
                                       -pyramid-scale 2 \
                                       -compression "JPEG-2000 Lossy" \
                                       -bigtiff \
                                       $file_in \
                                       $file_out
done <process_list.csv

